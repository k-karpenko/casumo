Casumo Rental Shop

Version: 1.0-SNAPSHOT
Language: Java 8

Implementation of REST API:
- Spring Web MVC
- FastXML (Jackson)

Other technologies:
- JODA Time

Server:
- Jetty 9.3.0.M1
- http://localhost:8081

Notes:

- Everything has been written with an immutability and testability in mind
- I've decided not to use JPA or other ORM frameworks to save a time, due to strick time constraint
- As I know, it is more preferable to use HTTP-codes in REST to highlight semantic of the particular error, but it could
 be easily done in *com.casumo.rental.web.utils.GlobalExceptionHandler*
- I did only manual testing of the resulted application with 'Advanced REST Client' extension for Chrome
- To write tests I would probably use Mockito + JUnit + Spring Tests (spring context in-test and Web MVC mocking)

To test REST API:

- Start embedded Jetty server (*com.casumo.web.Runner*) or deploy resulted WAR into the Application Server

[GET /api/customer] gives you a JSON-version of customers list exists in database (they are loading on startup)
[GET /api/product/{id}] - returns JSON-version of details of the particular product
[GET /api/product] - returns JSON-version of products list exists in database (they are loading on startup)
[POST /api/order @customerId @productIds[] @orderedTime @days] creates new order and draw all required receipts
[GET /api/order @customerId]
[GET /api/order/{id}] - returns details of the particular order
[GET /api/return/{returnId}] - returns JSON-version of products list exists in database (they are loading on startup)
[POST /api/return/{orderId}] - process return operation on given orderId

[GET /api/customer/{id}]


- Protocol format:

error:
   message: String
   code: Integer

result: Any

Error codes:
1003 - NOT_ALLOWED
1002 - NOT_FOUND
1001 - INTERNAL_EXCEPTION



