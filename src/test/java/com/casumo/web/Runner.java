package com.casumo.web;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.File;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class Runner {
    public static void main(String[] args) throws Exception {
        Runner runner = new Runner();
        runner.run();
    }

    public void run() throws Exception {
        Server server = new Server(8081);
        WebAppContext context = new WebAppContext();
        context.setLogUrlOnStart(true);
        context.setTempDirectory(new File("target/tmp"));
        context.setWar("src/main/webapp");
        context.setContextPath("/");
        context.setParentLoaderPriority(false);
        server.setHandler(context);

        server.start();
        server.join();
    }

}
