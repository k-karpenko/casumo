package com.casumo.rental.utils.serializers;

import com.casumo.rental.domain.Id;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.Optional;

public class OptionalDeserializer extends StdDeserializer<Optional<?>> {

    public OptionalDeserializer() {
        super(Optional.class);
    }

    @Override
    public Optional<?> deserialize(JsonParser json, DeserializationContext context) throws IOException {
        return Optional.ofNullable(json.getText());
    }

}