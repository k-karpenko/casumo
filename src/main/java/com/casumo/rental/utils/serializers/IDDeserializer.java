package com.casumo.rental.utils.serializers;

import com.casumo.rental.domain.Id;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class IDDeserializer extends StdDeserializer<Id<?>> {

    public IDDeserializer() {
        super(Id.class);
    }

    @Override
    public Id<?> deserialize(JsonParser json, DeserializationContext context) throws IOException {
        return new Id(json.getText());
    }

}