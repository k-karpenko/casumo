package com.casumo.rental.utils.serializers;

import com.casumo.rental.domain.Id;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.util.Optional;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class CasumoMapper extends ObjectMapper {
    public CasumoMapper() {
        SimpleModule module = new SimpleModule("JSONModule", new Version(2, 0, 0, null, null, null));
        module.addSerializer(Id.class, new IDSerializer());
        module.addDeserializer(Id.class, new IDDeserializer());
        module.addDeserializer(Optional.class, new OptionalDeserializer());
        module.addSerializer(Optional.class, new OptionalSerializer());
        registerModule(module);
    }
}
