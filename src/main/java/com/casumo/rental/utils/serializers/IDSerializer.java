package com.casumo.rental.utils.serializers;

import com.casumo.rental.domain.Id;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.text.DateFormat;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class IDSerializer extends StdSerializer<Id> {

    public IDSerializer() {
        super(Id.class);
    }

    @Override
    public void serialize(Id value, JsonGenerator json,
                          SerializerProvider provider) throws IOException {
        json.writeString(value.value());
    }

}
