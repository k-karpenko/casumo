package com.casumo.rental.utils.serializers;

import com.casumo.rental.domain.Id;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Optional;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class OptionalSerializer extends StdSerializer<Optional> {

    public OptionalSerializer() {
        super(Optional.class);
    }

    @Override
    public void serialize(Optional value, JsonGenerator json,
                          SerializerProvider provider) throws IOException {
        json.writeString(String.valueOf(value.orElse(null)));
    }

}
