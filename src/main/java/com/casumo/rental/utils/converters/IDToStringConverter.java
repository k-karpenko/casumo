package com.casumo.rental.utils.converters;

import com.casumo.rental.domain.Id;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class IDToStringConverter implements Converter<Id<?>, String> {

    @Override
    public String convert(Id<?> source) {
        return source.value();
    }

}