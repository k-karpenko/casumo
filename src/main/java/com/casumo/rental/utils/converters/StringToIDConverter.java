package com.casumo.rental.utils.converters;

import com.casumo.rental.domain.Id;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class StringToIDConverter implements Converter<String, Id> {

    @Override
    public Id<?> convert(String source) {
        return new Id(source);
    }

}