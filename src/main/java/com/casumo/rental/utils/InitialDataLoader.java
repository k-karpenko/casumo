package com.casumo.rental.utils;

import com.casumo.rental.domain.ProductType;
import com.casumo.rental.business.services.BusinessLogicException;
import com.casumo.rental.business.services.CustomerService;
import com.casumo.rental.business.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class InitialDataLoader {

    @Autowired
    CustomerService customerService;

    @Autowired
    ProductService productService;

    @PostConstruct
    public void loadInitialData() throws BusinessLogicException {
        loadInitialCustomers();
        loadInitialProducts();
    }

    public void loadInitialCustomers() throws BusinessLogicException {
        customerService.createCustomer("test");
        customerService.createCustomer("test 22");
        customerService.createCustomer("test 23");
    }

    public void loadInitialProducts() throws BusinessLogicException {
        productService.createProduct("Matrix 2", ProductType.NEW);
        productService.createProduct("Matrix", ProductType.NEW);
        productService.createProduct("Spider Man", ProductType.REGULAR);
        productService.createProduct("Spider Man II", ProductType.OLD);
    }

}
