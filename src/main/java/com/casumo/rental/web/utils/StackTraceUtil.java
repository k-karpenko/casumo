package com.casumo.rental.web.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public class StackTraceUtil {

    public static StringBuilder getString(Throwable ex) {
        StringWriter writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        ex.printStackTrace(printWriter);
        return new StringBuilder(writer.getBuffer());
    }

    public static StringBuilder getStringA5000(Throwable ex) {
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement st : ex.getStackTrace()) {
            if (st.getClassName().startsWith("com.casumo")) {
                sb.append("\tat ").append(st).append(System.lineSeparator());
            }
        }
        if (sb.length() > 0) {
            sb.append("\tOnly classes com.casumo.* included above");
        }
        return sb;
    }


}