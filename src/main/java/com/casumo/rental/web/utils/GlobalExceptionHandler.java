package com.casumo.rental.web.utils;

import com.casumo.rental.business.services.BusinessLogicException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;
import static org.springframework.web.servlet.HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
@ControllerAdvice
@Component
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    public static final String LS = System.lineSeparator();

    @ExceptionHandler(BusinessLogicException.class)
    public ResponseEntity<Map<String, Object>> handleBusinessLogicException(
            BusinessLogicException ex, WebRequest request) {
        logger.info(BusinessLogicException.class.getCanonicalName() + String.format(" (code %s): %s", ex.getCode(), ex.getMessage()) + LS + "\tat " + ex.getStackTrace()[0] + LS + getWebRequestDescription(request) + LS + getWebRequestParameters(request));
        clearProducibleMediaTypes(request);
        return new ResponseEntity(
            new RestExceptionResult(ex.getCode(), ex.getMessage()).map(),
            HttpStatus.OK
        );
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Map<String, Object>> handleAccessDeniedException(
            AccessDeniedException ex, WebRequest request) {
        logger.error( ex.getMessage(), ex );
        logger.info(ex.toString() + LS + StackTraceUtil.getStringA5000(ex) + LS + getWebRequestDescription(request));
        clearProducibleMediaTypes(request);
        return new ResponseEntity(
            new RestExceptionResult(HttpStatus.FORBIDDEN.value(), ex.getMessage()).map(),
            HttpStatus.FORBIDDEN
        );
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
            Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.warn(String.valueOf(StackTraceUtil.getString(ex)) + getWebRequestDescription(request) + LS + getWebRequestParameters(request));
        RestExceptionResult exceptionResult = new RestExceptionResult(status.value(), ex.getMessage());
        clearProducibleMediaTypes(request);
        return super.handleExceptionInternal(ex, exceptionResult.map(), headers, status, request);
    }

    @ExceptionHandler
    public ResponseEntity<Map<String, Object>> handleOtherException(Exception ex, WebRequest request) {
        logger.error(String.valueOf(StackTraceUtil.getString(ex)) + getWebRequestDescription(request) + LS + getWebRequestParameters(request));
        clearProducibleMediaTypes(request);
        return new ResponseEntity<>(
                new RestExceptionResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage()).map(),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }


    protected String getWebRequestDescription(WebRequest request) {
        return "Request info: " + request.getDescription(true);
    }

    protected StringBuilder getWebRequestParameters(WebRequest request) {
        StringBuilder sb = new StringBuilder();
        if (request.getParameterMap().size() > 0) {
            sb.append("Request parameters:");
            for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
                sb.append(LS).append("\t")
                        .append(entry.getKey())
                        .append(":")
                        .append(Arrays.asList(entry.getValue()).stream().collect(Collectors.joining(", ")));
            }
        } else {
            sb.append("No request parameters");
        }
        return sb;
    }

    @SuppressWarnings("unchecked")
    protected void clearProducibleMediaTypes(WebRequest request) {
        Set<MediaType> mediaTypes = (Set<MediaType>) request.getAttribute(PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE, SCOPE_REQUEST);
        if (!CollectionUtils.isEmpty(mediaTypes)) {
            mediaTypes.clear();
        }
    }

}
