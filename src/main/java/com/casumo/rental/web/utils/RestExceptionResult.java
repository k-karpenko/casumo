package com.casumo.rental.web.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class RestExceptionResult {
    private final String message;
    private final int code;

    public RestExceptionResult(int code, String message) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public Map<String, Object> map() {
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> error = new HashMap<>();
        error.put("message", message);
        error.put("code", code);
        result.put("error", error);
        return result;
    }
}
