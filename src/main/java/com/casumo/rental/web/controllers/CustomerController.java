package com.casumo.rental.web.controllers;

import com.casumo.rental.business.services.BusinessLogicException;
import com.casumo.rental.business.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
@Controller("CustomerController")
public class CustomerController extends AbstractController {

    @Autowired
    CustomerService customerService;

    @RequestMapping( value = "/customer", method = RequestMethod.GET )
    public @ResponseBody
    Map<String, Object> handleListRequest() throws BusinessLogicException {
        return result(customerService.getCustomers());
    }

}
