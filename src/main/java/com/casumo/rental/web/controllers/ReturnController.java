package com.casumo.rental.web.controllers;

import com.casumo.rental.business.services.BusinessLogicException;
import com.casumo.rental.business.services.OrderService;
import com.casumo.rental.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
@Controller("ReturnController")
public class ReturnController extends AbstractController {

    @Autowired
    OrderService orderService;

    @RequestMapping(value = "/return/{returnId}", method = RequestMethod.GET )
    public @ResponseBody Map<String, Object> handleReturnById( @PathVariable("returnId") Id<Return> orderId)
            throws BusinessLogicException {
        return result(orderService.getReturn(orderId));
    }

    @RequestMapping(value = "/return/{orderId}", method = RequestMethod.POST )
    public @ResponseBody Map<String, Object> handleReturnRequest( @PathVariable("orderId") Id<Order> orderId)
            throws BusinessLogicException {
        return result(orderService.returnRented(orderId));
    }

    @RequestMapping(value = "/return", method = RequestMethod.GET )
    public @ResponseBody
    Map<String, Object> handleReturnsRequest(@RequestParam("customerId") Id<Customer> customerId )
            throws BusinessLogicException {
        return result(orderService.getReturns(customerId));
    }
}
