package com.casumo.rental.web.controllers;

import com.casumo.rental.domain.*;
import com.casumo.rental.business.services.BusinessLogicException;
import com.casumo.rental.business.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
@Controller("OrderController")
public class OrderController extends AbstractController {

    @Autowired
    OrderService orderService;

    @RequestMapping(value = "/order", method = RequestMethod.POST )
    public @ResponseBody
    Map<String, Object> handlePostOrder(
                            @RequestParam("customerId") Id<Customer> customerId,
                            @RequestParam("productIds") List<String> productIds,
                            @RequestParam("orderedTime") Long orderedTime,
                            @RequestParam("days") Integer days)
        throws BusinessLogicException {
        return result(
            orderService.orderRent( customerId,
                productIds.stream().map(Id<Product>::new).collect(Collectors.toList()),
                new Date(orderedTime),
                days
            )
        );
    }

    @RequestMapping(value = "/order", method = RequestMethod.GET )
    public @ResponseBody Map<String, Object> handlePostOrder( @RequestParam("customerId") Id<Customer> customerId)
            throws BusinessLogicException {
        return result(orderService.getOrders(customerId));
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET )
    public @ResponseBody Map<String, Object> handleRequestById(@PathVariable("id") Id<Order> id)
            throws BusinessLogicException {
        return result(orderService.getOrder(id));
    }

}
