package com.casumo.rental.web.controllers;

import com.casumo.rental.domain.Product;
import com.casumo.rental.business.services.BusinessLogicException;
import com.casumo.rental.business.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller("ProductController")
public class ProductController extends AbstractController {

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/product", method = RequestMethod.GET )
    public @ResponseBody
    Map<String, Object> handlePostOrder() throws BusinessLogicException {
        return result(productService.getProducts());
    }

}
