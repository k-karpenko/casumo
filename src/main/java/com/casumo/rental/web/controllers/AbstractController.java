package com.casumo.rental.web.controllers;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractController {

	protected Map<String, Object> fail() {
		return result(false);
	}

	protected Map<String, Object> success() {
		return result(true);
	}

	protected Map<String, Object> result(Object value) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (value != null) {
			result.put("result", value);
		}

		return result;
	}

}