package com.casumo.rental.domain;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public enum ProductType {
    NEW,
    REGULAR,
    OLD;
}
