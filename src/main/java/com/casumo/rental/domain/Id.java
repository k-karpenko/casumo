package com.casumo.rental.domain;

import java.util.UUID;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 * @param <T>
 */
public class Id<T> {
    private final String value;

    public Id() {
        this(UUID.randomUUID().toString());
    }

    public Id(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Id<?> id = (Id<?>) o;

        return value.equals(id.value);

    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return value;
    }
}
