package com.casumo.rental.domain;

import java.math.BigDecimal;
import java.util.Date;

public class Receipt {
    private final ReceiptType type;
    private final Id<Receipt> id;
    private final Id<Customer> customer;
    private final Id<Receipt> parentReceipt;
    private final BigDecimal amount;
    private final Date createdDate;
    private final String details;

    public Receipt(ReceiptType type, Id<Receipt> id, BigDecimal amount, Date createdDate, String details,
                   Id<Customer> customer) {
        this(type, id, amount, createdDate, details, customer, null);
    }

    public Receipt(ReceiptType type, Id<Receipt> id, BigDecimal amount, Date createdDate, String details,
                   Id<Customer> customer, Id<Receipt> parentReceipt) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.createdDate = createdDate;
        this.details = details;
        this.customer = customer;
        this.parentReceipt = parentReceipt;
    }

    public Id<Receipt> getId() {
        return id;
    }

    public Id<Receipt> getParentReceipt() {
        return parentReceipt;
    }

    public Id<Customer> getCustomer() {
        return customer;
    }

    public ReceiptType getType() {
        return type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getDetails() {
        return details;
    }
}
