package com.casumo.rental.domain;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class Product {
    private final Id<Product> id;
    private final String name;
    private final ProductType type;

    public Product(Id<Product> id, String name, ProductType type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public Id<Product> getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ProductType getType() {
        return type;
    }

}
