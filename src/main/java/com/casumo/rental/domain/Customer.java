package com.casumo.rental.domain;

public class Customer {
    private final Id<Customer> id;
    private final String fullName;

    public Customer(Id<Customer> id, String fullName) {
        this.id = id;
        this.fullName = fullName;
    }

    public Id<Customer> getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }
}
