package com.casumo.rental.domain;

import java.util.*;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class Order {
    private final Id<Order> id;
    private final Id<Customer> customer;
    private final Date rentedDate;
    private final Integer days;
    private final Collection<Id<Product>> products = new ArrayList<>();
    private final List<Receipt> receipts = new ArrayList<>();

    public Order(Id<Order> id, Id<Customer> customer, Date rentedDate, List<Id<Product>> products, Integer days) {
        this.id = id;
        this.rentedDate = rentedDate;
        this.customer = customer;
        this.days = days;
        this.products.addAll(products);
    }

    public Id<Customer> getCustomer() {
        return customer;
    }

    public Id<Order> getId() {
        return id;
    }

    public Integer getDays() {
        return days;
    }

    public Collection<Id<Product>> getProducts() {
        return products;
    }

    public Date getRentedDate() {
        return rentedDate;
    }

    public void addReceipts(List<Receipt> receipts) {
        this.receipts.addAll(receipts);
    }

    public List<Receipt> getReceipts() {
        return receipts;
    }

    public void addReceipt(Receipt surchargesReceipt) {
        this.receipts.add(surchargesReceipt);
    }
}
