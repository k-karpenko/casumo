package com.casumo.rental.domain;

import java.util.Date;
import java.util.Optional;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class Return {
    private final Id<Return> id;
    private final Id<Order> order;
    private final Id<Customer> customer;
    private final Optional<Id<Receipt>> surchargesReceipt;
    private final Date returnDate;
    private final ReturnStatus status;

    public Return(Id<Return> id, Id<Order> order,
                  Id<Customer> customer, Optional<Id<Receipt>> surchargesReceipt,
                  Date returnDate, ReturnStatus status) {
        this.id = id;
        this.order = order;
        this.customer = customer;
        this.surchargesReceipt = surchargesReceipt;
        this.returnDate = returnDate;
        this.status = status;
    }

    public Optional<Id<Receipt>> getSurchargesReceipt() {
        return surchargesReceipt;
    }

    public Id<Customer> getCustomer() {
        return customer;
    }

    public Id<Return> getId() {
        return id;
    }

    public Id<Order> getOrder() {
        return order;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public ReturnStatus getStatus() {
        return status;
    }
}
