package com.casumo.rental.business.services.impl;

import com.casumo.rental.business.services.BusinessLogicException;
import com.casumo.rental.business.services.PriceCalculationService;
import com.casumo.rental.business.services.ProductService;
import com.casumo.rental.business.strategy.price.PriceCalculationStrategy;
import com.casumo.rental.domain.Order;
import com.casumo.rental.domain.ProductType;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class PriceCalculationServiceImpl implements PriceCalculationService {

    private final Map<ProductType, PriceCalculationStrategy> strategies = new HashMap<>();

    @Autowired
    ProductService productService;

    public PriceCalculationServiceImpl(Map<ProductType, PriceCalculationStrategy> strategiesMap ) {
        strategies.putAll(strategiesMap);
    }

    @Override
    public BigDecimal calculateOrderPrice(Order order, int days) throws BusinessLogicException {
        return order.getProducts().stream()
                .map(productService::getProduct)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(p -> strategies.get(p.getType()).calculateOrderPrice(days) )
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
