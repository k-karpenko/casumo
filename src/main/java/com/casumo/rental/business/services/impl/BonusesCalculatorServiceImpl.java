package com.casumo.rental.business.services.impl;

import com.casumo.rental.business.services.BonusesCalculatorService;
import com.casumo.rental.business.services.BusinessLogicException;
import com.casumo.rental.business.services.ProductService;
import com.casumo.rental.business.strategy.bonuses.BonusCalculationStrategy;
import com.casumo.rental.domain.Order;
import com.casumo.rental.domain.ProductType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class BonusesCalculatorServiceImpl implements BonusesCalculatorService {

    @Autowired
    ProductService productService;

    private final Map<ProductType, BonusCalculationStrategy> bonusPointValues = new HashMap<>();

    public BonusesCalculatorServiceImpl(Map<ProductType, BonusCalculationStrategy> map) {
        bonusPointValues.putAll(map);
    }

    @Override
    public BigDecimal calculateBonusPoints(Order order) throws BusinessLogicException {
        return
            order.getProducts().stream()
                .map(productService::getProduct)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(p -> bonusPointValues.get(p.getType()).calculateBonusPoints(p, order))
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

}
