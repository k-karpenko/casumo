package com.casumo.rental.business.services;

import com.casumo.rental.domain.*;

import java.math.BigDecimal;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public interface ReceiptService {

    Receipt issueReceipt(ReceiptType type,
                         Id<Customer> customer, BigDecimal amount, String details) throws BusinessLogicException;

    Receipt issueBonusReceipt(ReceiptType type, Id<Customer> customer, BigDecimal amount, String details,
                              Id<Receipt> originalReceipt) throws BusinessLogicException;

}
