package com.casumo.rental.business.services.impl;

import com.casumo.rental.domain.*;
import com.casumo.rental.business.services.BusinessLogicException;
import com.casumo.rental.business.services.ReceiptService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
@Service("ReceiptServiceImpl")
@Scope("singleton")
public class ReceiptServiceImpl implements ReceiptService {
    private final List<Receipt> receipts = new Vector<>();
    private final List<Receipt> bonusReceipts = new Vector<>();

    @Override
    public Receipt issueReceipt(ReceiptType type, Id<Customer> customer, BigDecimal amount, String details)
            throws BusinessLogicException {
        Receipt receipt;
        receipts.add(receipt = new Receipt(type, new Id<Receipt>(), amount, new Date(), details, customer) );
        return receipt;
    }

    @Override
    public Receipt issueBonusReceipt(ReceiptType type, Id<Customer> customer, BigDecimal amount, String details,
                                     Id<Receipt> originalReceipt) throws BusinessLogicException {
        Receipt receipt;
        bonusReceipts.add(
            receipt = new Receipt(type, new Id<Receipt>(), amount, new Date(), details, customer,
                    originalReceipt)
        );
        return receipt;
    }
}
