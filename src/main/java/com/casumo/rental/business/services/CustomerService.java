package com.casumo.rental.business.services;

import com.casumo.rental.domain.Customer;
import com.casumo.rental.domain.Id;

import java.util.List;
import java.util.Optional;

public interface CustomerService {

    Customer createCustomer(String fullName) throws BusinessLogicException;

    Optional<Customer> getCustomer(Id<Customer> customerId) throws BusinessLogicException;

    List<Customer> getCustomers() throws BusinessLogicException;

    boolean checkExists(Id<Customer> customerId) throws BusinessLogicException;
}
