package com.casumo.rental.business.services;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class BusinessLogicException extends RuntimeException {
    private final int code;

    public BusinessLogicException(int code) {
        this(code, "");
    }

    public BusinessLogicException(int code, String message) {
        super(message);

        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
