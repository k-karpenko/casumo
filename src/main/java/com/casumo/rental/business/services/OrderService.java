package com.casumo.rental.business.services;

import com.casumo.rental.domain.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public interface OrderService {

    Optional<Order> getOrder(Id<Order> orderId ) throws BusinessLogicException;

    Order orderRent(Id<Customer> customerId, List<Id<Product>> productIds, Date orderedDate, Integer days)
            throws BusinessLogicException;

    Return returnRented( Id<Order> orderId ) throws BusinessLogicException;

    List<Order> getOrders(Id<Customer> customerId) throws BusinessLogicException;

    List<Return> getReturns(Id<Customer> customerId) throws BusinessLogicException;

    Return getReturn(Id<Return> returnId) throws BusinessLogicException;
}
