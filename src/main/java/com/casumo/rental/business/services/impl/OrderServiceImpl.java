package com.casumo.rental.business.services.impl;

import com.casumo.rental.business.services.*;
import com.casumo.rental.business.services.PriceCalculationService;
import com.casumo.rental.domain.*;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service("OrderingServiceImpl")
@Scope("singleton")
public class OrderServiceImpl implements OrderService {

    private final List<Order> orders = new Vector<>();
    private final List<Id<Order>> completeOrders = new Vector<>();
    private final List<Return> returns = new Vector<>();

    private final Object returnLock = new Object();

    @Value("${receipt.surcharges.details}")
    private String surchargesDetailsMessage;

    @Value("${receipt.order.details}")
    private String orderDetailsMessage;

    @Value("${receipt.bonuses.income.details}")
    private String bonusesIncomeDetailsMessage;

    @Autowired
    ProductService productService;

    @Autowired
    CustomerService customerService;

    @Autowired
    ReceiptService receiptService;

    @Autowired
    PriceCalculationService priceCalculationService;

    @Autowired
    BonusesCalculatorService bonusesCalculatorService;

    @Override
    public Optional<Order> getOrder(Id<Order> orderId) throws BusinessLogicException {
        return orders.parallelStream()
              .filter(p -> p.getId().equals(orderId))
                .findFirst();
    }

    @Override
    public Order orderRent(Id<Customer> customerId, List<Id<Product>> productIds, Date orderedDate, Integer days)
            throws BusinessLogicException {
        if ( productIds.isEmpty() || !productService.checkExists(productIds) ) {
            throw new BusinessLogicException(BusinessLogicExceptionCode.NOT_FOUND, "product");
        }

        if ( !customerService.checkExists(customerId) ) {
            throw new BusinessLogicException(BusinessLogicExceptionCode.NOT_FOUND, "customer");
        }

        Order result = new Order(new Id<>(), customerId, orderedDate, productIds, days );

        List<Receipt> receipts = new ArrayList<>();

        Receipt baseReceipt;
        receipts.add(
            baseReceipt = receiptService.issueReceipt(ReceiptType.CHARGE, customerId,
                priceCalculationService.calculateOrderPrice(result, days), prepareReceiptDetails(result))
        );

        BigDecimal bonusPoints = bonusesCalculatorService.calculateBonusPoints(result);
        if ( !bonusPoints.equals(BigDecimal.ZERO) ) {
            receipts.add(
                receiptService.issueBonusReceipt(ReceiptType.INCOME, customerId, bonusPoints,
                    prepareBonusesIncomeDetails(bonusPoints, result), baseReceipt.getId() )
            );
        }

        result.addReceipts(receipts);

        orders.add(result);

        return result;
    }

    @Override
    public Return returnRented(Id<Order> orderId) throws BusinessLogicException {
        synchronized (orderId.value().intern()) {
            Order order = orders.stream()
                    .filter(p -> p.getId().equals(orderId))
                    .findFirst()
                    .orElseThrow(() -> new BusinessLogicException(BusinessLogicExceptionCode.NOT_FOUND));
            if (completeOrders.contains(order.getId())) {
                throw new BusinessLogicException(BusinessLogicExceptionCode.NOT_ALLOWED, "already.processed");
            } else {
                completeOrders.add(order.getId());
            }

            LocalDate expirationDate = LocalDate.fromDateFields(order.getRentedDate()).plusDays(order.getDays());
            Days extraDays = expirationDate.isAfter(LocalDate.now()) ? Days.ZERO :
                    Days.daysBetween( expirationDate, LocalDate.now() );

            ReturnStatus returnStatus;
            Optional<Id<Receipt>> surchargesReceiptId;
            if (extraDays.isGreaterThan(Days.ONE)) {
                Receipt surchargesReceipt = receiptService.issueReceipt(ReceiptType.CHARGE, order.getCustomer(),
                        priceCalculationService.calculateOrderPrice(order, extraDays.getDays()),
                        prepareSurchargesDetails(extraDays.getDays(), order));
                surchargesReceiptId = Optional.of(surchargesReceipt.getId());
                order.addReceipt(surchargesReceipt);

                returnStatus = ReturnStatus.SURCHARGE;
            } else {
                returnStatus = ReturnStatus.NORMAL;
                surchargesReceiptId = Optional.empty();
            }

            Return result;
            returns.add(
                    result = new Return(new Id<>(), order.getId(), order.getCustomer(), surchargesReceiptId,
                            new Date(), returnStatus)
            );
            return result;
        }
    }

    @Override
    public List<Order> getOrders(Id<Customer> customerId) throws BusinessLogicException {
        if ( !customerService.checkExists(customerId) ) {
            throw new BusinessLogicException(BusinessLogicExceptionCode.NOT_FOUND, "customer");
        }

        return orders.stream()
                .filter(o -> o.getCustomer().equals(customerId) )
            .collect(Collectors.toList());
    }

    @Override
    public List<Return> getReturns(Id<Customer> customerId) throws BusinessLogicException {
        if ( !customerService.checkExists(customerId) ) {
            throw new BusinessLogicException(BusinessLogicExceptionCode.NOT_FOUND, "customer");
        }

        return returns.stream()
                .filter(r -> r.getCustomer().equals(customerId) )
            .collect(Collectors.toList());
    }

    @Override
    public Return getReturn(Id<Return> returnId) throws BusinessLogicException {
        return returns.stream()
                .filter(r -> r.getId().equals(returnId) )
                .findFirst()
            .orElseThrow(() -> new BusinessLogicException(BusinessLogicExceptionCode.NOT_FOUND, "return"));
    }

    protected String prepareSurchargesDetails( int days, Order order ) {
        return String.format(surchargesDetailsMessage, order.getId(), days);
    }

    protected String prepareReceiptDetails( Order order ) {
        return String.format(orderDetailsMessage, order.getId());
    }

    protected String prepareBonusesIncomeDetails( BigDecimal bonusPoints, Order order ) {
        return String.format(bonusesIncomeDetailsMessage, bonusPoints.toString(), order.getId());
    }

}
