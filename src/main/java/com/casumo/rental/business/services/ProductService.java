package com.casumo.rental.business.services;

import com.casumo.rental.domain.Id;
import com.casumo.rental.domain.Product;
import com.casumo.rental.domain.ProductType;

import java.util.List;
import java.util.Optional;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public interface ProductService {

    Optional<Product> getProduct(Id<Product> id);

    List<Product> getProducts() throws BusinessLogicException;

    Product createProduct(String productName, ProductType productType) throws BusinessLogicException;

    boolean checkExists(List<Id<Product>> productIds) throws BusinessLogicException;
}
