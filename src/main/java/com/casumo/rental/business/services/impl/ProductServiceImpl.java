package com.casumo.rental.business.services.impl;

import com.casumo.rental.domain.Id;
import com.casumo.rental.domain.Product;
import com.casumo.rental.domain.ProductType;
import com.casumo.rental.business.services.BusinessLogicException;
import com.casumo.rental.business.services.ProductService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
@Service("ProductServiceImpl")
@Scope("singleton")
public class ProductServiceImpl implements ProductService {

    private final List<Product> products = new ArrayList<Product>();

    @Override
    public Optional<Product> getProduct(Id<Product> id) {
        return products.stream()
            .filter( p -> p.getId().equals(id) )
            .findFirst();
    }

    @Override
    public List<Product> getProducts() throws BusinessLogicException {
        return Collections.unmodifiableList(products);
    }

    @Override
    public Product createProduct(String productName, ProductType productType) throws BusinessLogicException {
        Product product;
        products.add(product = new Product(new Id<>(), productName, productType));
        return product;
    }

    @Override
    public boolean checkExists(List<Id<Product>> productIds) throws BusinessLogicException {
        return productIds.size() == productIds.stream()
                .map(id -> getProduct(id))
                .filter(Optional::isPresent)
                .count();
    }
}
