package com.casumo.rental.business.services.impl;

import com.casumo.rental.business.services.BusinessLogicExceptionCode;
import com.casumo.rental.domain.Customer;
import com.casumo.rental.business.services.BusinessLogicException;
import com.casumo.rental.business.services.CustomerService;
import com.casumo.rental.domain.Id;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
@Service("CustomerServiceImpl")
@Scope("singleton")
public class CustomerServiceImpl implements CustomerService {

    private final List<Customer> customers = new Vector<Customer>();

    @Override
    public Optional<Customer> getCustomer(Id<Customer> customerId) throws BusinessLogicException {
        return customers.stream()
                .filter( c -> c.getId().equals(customerId) )
            .findFirst();
    }

    @Override
    public List<Customer> getCustomers() throws BusinessLogicException {
        return Collections.unmodifiableList(customers);
    }

    @Override
    public boolean checkExists(Id<Customer> customerId) throws BusinessLogicException {
        return this.getCustomer(customerId).isPresent();
    }

    @Override
    public Customer createCustomer(String fullName) throws BusinessLogicException {
        Customer customer;
        customers.add(customer = new Customer(new Id<>(), fullName));

        return customer;

    }
}
