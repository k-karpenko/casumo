package com.casumo.rental.business.services;

import com.casumo.rental.domain.Order;
import com.casumo.rental.domain.Product;

import java.math.BigDecimal;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public interface BonusesCalculatorService {

    BigDecimal calculateBonusPoints(Order order) throws BusinessLogicException;

}
