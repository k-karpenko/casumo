package com.casumo.rental.business.services;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public final class BusinessLogicExceptionCode {
    public static final int INTERNAL_EXCEPTION = 1001;
    public static final int NOT_FOUND = 1002;
    public static final int NOT_ALLOWED = 1003;
}
