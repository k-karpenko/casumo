package com.casumo.rental.business.strategy.price;

import java.math.BigDecimal;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class NewProductPriceCalculationStrategy extends AbstractPriceCalculationStrategy {

    @Override
    public BigDecimal calculateOrderPrice(int days) {
        return getRentingPrice().multiply(BigDecimal.valueOf(days));
    }

}
