package com.casumo.rental.business.strategy.price;

import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public abstract class AbstractPriceCalculationStrategy implements PriceCalculationStrategy {

    @Value("${product.price}")
    Float price;

    protected BigDecimal getRentingPrice() {
        return BigDecimal.valueOf(price);
    }

}
