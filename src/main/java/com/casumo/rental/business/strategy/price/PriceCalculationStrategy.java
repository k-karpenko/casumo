package com.casumo.rental.business.strategy.price;

import com.casumo.rental.domain.ProductType;

import java.math.BigDecimal;

public interface PriceCalculationStrategy {

    BigDecimal calculateOrderPrice(int days);

}
