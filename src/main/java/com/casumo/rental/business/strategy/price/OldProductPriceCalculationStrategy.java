package com.casumo.rental.business.strategy.price;

import org.joda.time.Days;

import java.math.BigDecimal;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class OldProductPriceCalculationStrategy extends AbstractPriceCalculationStrategy {

    @Override
    public BigDecimal calculateOrderPrice(int days) {
        return getRentingPrice().add(
            getRentingPrice().multiply( BigDecimal.valueOf(Days.days(days).minus(5).getDays()) )
        );
    }
}
