package com.casumo.rental.business.strategy.bonuses;

import com.casumo.rental.domain.Order;
import com.casumo.rental.domain.Product;

import java.math.BigDecimal;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class SimpleBonusCalculationStrategy implements BonusCalculationStrategy {

    private final BigDecimal byDayBonus;

    public SimpleBonusCalculationStrategy(Float byDayBonus) {
        this.byDayBonus = BigDecimal.valueOf(byDayBonus);
    }

    @Override
    public BigDecimal calculateBonusPoints(Product product, Order order) {
        return byDayBonus.multiply( BigDecimal.valueOf(order.getDays()) );
    }
}
