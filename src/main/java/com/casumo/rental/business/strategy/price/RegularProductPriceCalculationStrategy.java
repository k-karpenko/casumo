package com.casumo.rental.business.strategy.price;

import org.joda.time.Days;

import java.math.BigDecimal;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public class RegularProductPriceCalculationStrategy extends AbstractPriceCalculationStrategy {

    @Override
    public BigDecimal calculateOrderPrice(int days) {
        return getRentingPrice().add(
            BigDecimal.valueOf(Days.days(days).minus(3).getDays()).multiply(getRentingPrice())
        );
    }

}
