package com.casumo.rental.business.strategy.bonuses;

import com.casumo.rental.domain.Order;
import com.casumo.rental.domain.Product;

import java.math.BigDecimal;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
public interface BonusCalculationStrategy {

    BigDecimal calculateBonusPoints(Product product, Order order);

}
